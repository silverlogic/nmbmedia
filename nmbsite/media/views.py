from django.shortcuts import render
from . import forms
from django.core.mail import send_mail
from django.contrib import messages

# new imports that go at the top of the file
from django.core.mail import EmailMessage
from django.shortcuts import redirect
from django.template import Context
from django.template.loader import get_template
from django.http import FileResponse, Http404


def pdf_view(request):
    return render(request, 'media/kit.html')


def home(request):
    return render(request, 'media/home.html')


def about(request):
    return render(request, 'media/about.html')


def whyooh(request):
    return render(request, 'media/why_ooh.html')


def facebook(request):
    return render(request, 'media/facebook_video.html')


def instagram(request):
    return render(request, 'media/instagram_video.html')


def design_specs(request):
    return render(request, 'media/design_specs.html')


def adbike_pdf(request):
    return render(request, 'media/design_spec_pdfs/adbike_pdf.html')


def aerial_inflatables_pdf(request):
    return render(request, 'media/design_spec_pdfs/aerial_inflatables_pdf.html')


def digital_mobile_pdf(request):
    return render(request, 'media/design_spec_pdfs/digital_mobile_pdf.html')


def digital_adbike_pdf(request):
    return render(request, 'media/design_spec_pdfs/digital_adbike_pdf.html')


def human_directionals_pdf(request):
    return render(request, 'media/design_spec_pdfs/human_directionals_pdf.html')


def mobile_billboards_pdf(request):
    return render(request, 'media/design_spec_pdfs/mobile_billboards_pdf.html')


def projection_media_pdf(request):
    return render(request, 'media/design_spec_pdfs/projection_media_pdf.html')


def roller_boards_pdf(request):
    return render(request, 'media/design_spec_pdfs/roller_boards_pdf.html')


def segways_pdf(request):
    return render(request, 'media/design_spec_pdfs/segways_pdf.html')


def walking_billboards_pdf(request):
    return render(request, 'media/design_spec_pdfs/walking_billboards_pdf.html')


def contact(request):
    form = forms.ContactForm()
    if request.method == 'POST':
        form = forms.ContactForm(request.POST)
        if form.is_valid():
            send_mail(
                'Message from nmbmedia.com',
                form.cleaned_data['message'],
                '{name} <{email}>'.format(**form.cleaned_data),
                ['frank@nmbmedia.com', 'sales@nmbmedia.com']
            )
            messages.add_message(request, messages.SUCCESS,
                                 'Thanks for your suggestion!')
            return redirect('media:contact')
    return render(request, 'media/contact.html', {'form': form})


def media(request):
    return render(request, 'media/media.html')


def clients(request):
    return render(request, 'media/clients.html')


def testimonials(request):
    return render(request, 'media/testimonials.html')


def adbikes(request):
    return render(request, 'media/platforms/adbikes.html')


def inflatables(request):
    return render(request, 'media/platforms/inflatables.html')


def wraps(request):
    return render(request, 'media/platforms/wraps.html')


def ambassadors(request):
    return render(request, 'media/platforms/ambassadors.html')


def banners(request):
    return render(request, 'media/platforms/banners.html')


def digital_adbikes(request):
    return render(request, 'media/platforms/digital_adbikes.html')


def fence_wraps(request):
    return render(request, 'media/platforms/fence_wraps.html')


def human_directionals(request):
    return render(request, 'media/platforms/human_directionals.html')


def mobile_billboards(request):
    return render(request, 'media/platforms/mobile_billboards.html')


def projection_media(request):
    return render(request, 'media/platforms/projection_media.html')


def roller_boards(request):
    return render(request, 'media/platforms/roller_boards.html')


def sculptures(request):
    return render(request, 'media/platforms/sculptures.html')


def segways(request):
    return render(request, 'media/platforms/segways.html')


def street_graphics(request):
    return render(request, 'media/platforms/street_graphics.html')


def walking_billboards(request):
    return render(request, 'media/platforms/walking_billboards.html')


def mobile_billboards_3D(request):
    return render(request, 'media/platforms/mobile_billboards_3D.html')


def digital_mobile_billboards(request):
    return render(request, 'media/platforms/digital_mobile_billboards.html')
