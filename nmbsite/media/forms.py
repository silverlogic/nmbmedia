from django import forms


class ContactForm(forms.Form):
    name = forms.CharField(required=True)
    email = forms.EmailField(required=True)
    message = forms.CharField(
        required=True,
        widget=forms.Textarea(attrs={'style': 'height: 135px;', 'class': 'form-control'}),
    )

    # the new bit we're adding
    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.fields['name'].label = "Name:"
        self.fields['email'].label = "Email:"
        self.fields['message'].label = "Message:"
